import React from 'react'
import { Container, Menu, Step } from 'semantic-ui-react'
import moment from 'moment';

const Head = ({messagesCount, participantsCount = '?', lastMessageDate = '?'}) => (
  <Menu fixed='top' inverted borderless size="lage">
    <Container text>
      <Step.Group widths={4} size='mini'>

        <Step title="Chat"  active icon="comment alternate outline" />

        <Step description="PARTICIPANTS" icon="users" title={participantsCount} />

        <Step description="MESSAGES" icon="address card outline" title={messagesCount} />

        <Step description="LAST MESSAGE" icon="clock outline" title={moment.utc(lastMessageDate).local().fromNow()} />

      </Step.Group>
    </Container> 
  </Menu>
)

export default Head;
