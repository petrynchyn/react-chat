import React, { useState, useCallback } from 'react'
import { Container, Form, Button } from 'semantic-ui-react'
import { v4 as uuidv4 } from 'uuid';

const MessageInput = ({ user, dispatch }) => {

  const [msgText, setMsgText] = useState('')

  const sendMsgHandler = useCallback(
    () => {
      let newMsg = {
        id: uuidv4(),
        text: msgText,
        likes: 0,
        editedAt: '',
        createdAt: new Date().toISOString(),
        userId: user.userId,
        user: user.user,
        avatar: user.avatar
      }
      dispatch({ type: 'add', payload: newMsg })
      setMsgText('')
    }, [dispatch, user, msgText]
  )

  return (
    <Container text>
      <Form onSubmit={sendMsgHandler} widths="equal">
        <Form.Group>
          <Form.TextArea autoFocus value={msgText} onChange={ev => setMsgText(ev.target.value)} placeholder='Message' />
          <Button type='submit' disabled={!msgText.length} secondary>Send</Button>
        </Form.Group>
      </Form>
    </Container>
  )
}

export default MessageInput;
