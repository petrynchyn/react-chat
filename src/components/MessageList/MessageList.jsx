import React, { useState, useCallback } from 'react'
import { Container, Divider, Segment, Icon, Comment, Confirm, Form, Button } from 'semantic-ui-react'
import moment from 'moment';

const MessageList = ({ messages, uId, dispatch }) => {
  const [deleteMsgIndex, setDeleteMsgIndex] = useState(null)
  const [editMsgIndex, setEditMsgIndex] = useState(null)
  const [editedMsgText, setEditedMsgText] = useState()

  const deleteMsgHandler = useCallback(
    () => {
      dispatch({ type: 'delete', payload: deleteMsgIndex })
      setDeleteMsgIndex(null)
    }, [dispatch, deleteMsgIndex]
  )

  const saveMsgHandler = useCallback(
    () => {
      dispatch({ type: 'edit', payload: {editMsgIndex, editedMsgText} })
      setEditMsgIndex(null)
    }, [dispatch, editMsgIndex, editedMsgText]
  )

  return (
    <Container text style={{ marginTop: '7rem', marginBottom: '2rem'}}>

      {messages.map(({ id, user, userId, avatar, text, createdAt, editedAt, likes }, index, array) => {
        const curFromNow = moment.utc(createdAt).local().fromNow();
        const prevFromNow = moment.utc(array[index - 1]?.createdAt).local().fromNow();
        return (
          <div key={id} >
            {curFromNow !== prevFromNow && <Divider horizontal fitted clearing style={{ 'fontSize': '.7em' }}>{curFromNow}</Divider>}
            <Segment floated={uId === userId ? 'right' : 'left'} style={{ maxWidth: '52%', minWidth:'52%', marginLeft: '1rem', marginRight: '1rem' }}>
              <Comment.Group size="small">
                <Comment>
                  {uId !== userId && <Comment.Avatar src={avatar} />}
                  <Comment.Content>
                    <Comment.Author as="a">{user}</Comment.Author>
                    <Comment.Metadata>{editedAt && <Icon name="edit" corner="top right" />}</Comment.Metadata>
                    <Comment.Metadata>{moment.utc(editedAt || createdAt).local().format('llll')}</Comment.Metadata>
                    {editMsgIndex !== index && <Comment.Text>{text}</Comment.Text>}
                    {editMsgIndex === index
                      && <Form reply onSubmit={saveMsgHandler} onReset={() => setEditMsgIndex(null)}>
                          <Form.TextArea autoFocus value={editedMsgText} onChange={ev => setEditedMsgText(ev.target.value)} />
                          <Button.Group size="mini" compact floated="right">
                            <Button type="reset">Cancel</Button>
                            <Button.Or />
                            <Button type="submit" disabled={editedMsgText === text} secondary>Save</Button>
                          </Button.Group>
                      </Form>
                    }
                    {editMsgIndex !== index && <Comment.Actions>
                      <Comment.Action onClick={() => dispatch({ type: 'like', payload: index })}><Icon name="thumbs up" />{likes}</Comment.Action>
                      {uId === userId && <Comment.Action onClick={() => { setEditMsgIndex(index); setEditedMsgText(text) }}><Icon name="edit" />Edit</Comment.Action>}
                      {uId === userId && <Comment.Action onClick={() => setDeleteMsgIndex(index)}><Icon name="trash" />Delete</Comment.Action>}
                    </Comment.Actions>
                    }
                  </Comment.Content>
                </Comment>
              </Comment.Group>
            </Segment>

            <Divider hidden fitted clearing />
          </div>
        )
      })}
      <Confirm
        content='Are you sure want to delete this message?'
        cancelButton='Never mind'
        confirmButton="Let's do it"
        size='mini'
        open={deleteMsgIndex===null ? false : true}
        onCancel={() => setDeleteMsgIndex(null)}
        onConfirm={deleteMsgHandler}
      />
    </Container>
  )
};

export default MessageList;
