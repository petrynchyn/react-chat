import React, { useState, useEffect, useReducer } from 'react'
import { Container, Header, Segment, Loader } from 'semantic-ui-react'
import { getAllMessages } from "../../services/messageService";
import Head from "../Head/Head";
import MessageList from "../MessageList/MessageList";
import MessageInput from "../MessageInput/MessageInput";

function reducer(state, action) {
  switch (action.type) {

    case 'like':
      state[action.payload].likes = 1 + ( state[action.payload].likes || 0)
      return [...state]

    case 'add':
      return [...state, action.payload]

    case 'edit':
      state[action.payload.editMsgIndex].text = action.payload.editedMsgText
      state[action.payload.editMsgIndex].editedAt = new Date().toISOString()
      return [...state]

    case 'delete':
      state.splice(action.payload, 1)
      return [...state]

    case 'set':
      return action.payload

    default:
      throw new Error('action undefined')
  }
}

const Chat = () => {

  const [messages, dispatch] = useReducer(reducer, []);
  const [lastMessageDate, setLastMessageDate] = useState()
  const [participantsCount, setParticipantsCount] = useState()
  const [showLoading, setShowLoading] = useState(false)
  const [user, setUserId] = useState({})

  useEffect(() => {
    setShowLoading(true)
    getAllMessages().then(res => {
      dispatch({ type: 'set', payload: res })
      setUserId(res[0])
      setShowLoading(false)
    })
  }, [])

  useEffect(() => {
    const participants = new Set()
    messages.forEach(element => participants.add(element.userId))
    setParticipantsCount(participants.size);
    setLastMessageDate(messages[messages.length - 1]?.createdAt);
  }, [messages])

  
  return (
    <>
      <Loader active={showLoading}>Loading</Loader>

      <Head
        messagesCount={messages.length}
        participantsCount={participantsCount}
        lastMessageDate={lastMessageDate}
      />

      <MessageList messages={messages} uId={user?.userId} dispatch={dispatch} />
      
      <MessageInput user={user} dispatch={dispatch} />
      
      <Segment inverted vertical style={{ margin: '2em 0em 0em', padding: '2em 0em' }}>
        <Container textAlign="center">
          <Header inverted as="h4" content="&copy; ANDRII PETRYNCHYN" />
        </Container>
      </Segment>
    </>
  );
}


export default Chat;
