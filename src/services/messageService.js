export const getAllMessages = async () => {
  const response = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
  return response.json()
};